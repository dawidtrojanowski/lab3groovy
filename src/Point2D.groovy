class Point2D {

    private final double x
    private final double y

    Point2D(double x, double y) {
        this.x = x
        this.y = y
    }

    double getX() {
        return x
    }

    double getY() {
        return y
    }

    @Override
    String toString() {
        return "Point2D{" +
                "x=" + x +
                ", y=" + y +
                '}'
    }
}