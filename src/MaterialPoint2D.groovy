class MaterialPoint2D extends Point2D {
    private final double mass

    MaterialPoint2D(double x, double y, double mass) {
        super(x, y)
        this.mass = mass
    }

    double getMass() {
        return mass
    }

    @Override
    String toString() {
        return super.toString() + "MaterialPoint2D{" +
                "mass=" + mass +
                '}'
    }
}