class Calculations {
    static Point2D positionGeometricCenter(Point2D[] points) {
        double sumX = 0.0
        double sumY = 0.0
        int size = points.size()
        for (int i = 0; i < size; i++) {
            sumX += points[i].getX()
            sumY += points[i].getY()
        }
        return new Point2D(sumX / size, sumY / size)
    }

    static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoint) {
        double x = 0.0
        double y = 0.0
        double mass = 0.0
        for(int i = 0; i < materialPoint.length; i++) {
            x += materialPoint[i].getX() * materialPoint[i].getMass()
            y += materialPoint[i].getY() * materialPoint[i].getMass()
            mass += materialPoint[i].getMass()
        }

        return new MaterialPoint2D(x / mass, y / mass, mass)
    }
}
